/* eslint-disable */

console.log("repo is working");
function getRoles() {
  return axios.get("/api/role").then(res => res.data.roles);
}

function getGenders() {
  return axios.get("/api/gender").then(res => res.data.genders);
}
function getUsers() {
  return axios.get("/api/user").then(res => res.data.users);
}
// Helper function
//decode value of text area
function htmlDecode(value) {
  return $("<textarea/>")
    .html(value)
    .text();
}

//encode value of text area
function htmlEncode(value) {
  return $("<textarea/>")
    .text(value)
    .html();
}

function addIndexTo(data) {
  return data.map(d => (d = { index: data.indexOf(d) + 1, ...d }));
}

function activateAccount(id) {
  return axios.post("/api/user/activate", { id }).then(res => res.data.user);
}
function suspendAccount(id) {
  return axios.post("/api/user/suspend", { id });
}
