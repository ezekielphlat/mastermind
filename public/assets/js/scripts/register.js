$(document).ready(function() {
  // Form Fields
  var $firstName = $("#firstName");
  var $lastName = $("#lastName");
  var $email = $("#email");
  var $password = $("#password");
  var $phoneNumber = $("#phoneNumber");

  // Gender Field
  var $gender = $(".gender");
  var $gendersTemplate = $("#gendersTemplate");
  var $gendersContainer = $("#gendersContainer");

  //registration form data
  function regFormData() {
    return {
      firstName: $firstName.val(),
      lastName: $lastName.val(),
      email: $email.val(),
      password: $password.val(),
      phoneNumber: $phoneNumber.val(),
      gender: $gender.val()
    };
  }
  //Form
  var $registrationForm = $("#registrationForm");
  $registrationForm.submit(function(e) {
    e.preventDefault();

    console.log(regFormData());
  });
  getGenders().then(gendersData => {
    var template = Mustache.render($gendersTemplate.html(), {
      genders: gendersData
    });
    $gendersContainer.html(template);
  });

  // Get Gender
});
