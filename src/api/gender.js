const express = require("express");
const parseError = require("../utils/parseError");
const Gender = require("../models/Gender");

const router = express.Router();

router.get("/", (req, res) => {
  Gender.find({}).then(genders => {
    if (genders) {
      res.status(200).json({ genders });
      // console.log(genders);
    } else {
      res.status(400).json({ errors: parseError(err.errors) });
    }
  });
});
module.exports = router;
