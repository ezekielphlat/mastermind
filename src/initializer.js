const mongoose = require("mongoose");
const AppRunCount = require("./models/AppRunCount");
const Role = require("./models/Role");
const Gender = require("./models/Gender");
const User = require("./models/User");
const bcrypt = require("bcrypt");

module.exports = function initializer() {
  // Initialize gender
  const male = new Gender({
    _id: new mongoose.Types.ObjectId(),
    name: "male"
  });
  const female = new Gender({
    _id: new mongoose.Types.ObjectId(),
    name: "female"
  });

  // Initialize roles
  const superAdminRole = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "superadmin",
    description: "super admin role"
  });
  const adminRole = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "admin",
    description: "admin role"
  });
  const agentRole = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "agent",
    description: "agent role"
  });
  const investorRole = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "investor",
    description: "investor role"
  });

  // Initialize users
  const superAdmin = new User({
    _id: new mongoose.Types.ObjectId(),
    email: "admin@admin.com",
    passwordHash: bcrypt.hashSync("passwordIsAdmin", 10),
    role: superAdminRole._id,
    active: true,
    confirmed: true
  });

  const fistTimeRun = new AppRunCount({
    _id: new mongoose.Types.ObjectId(),
    count: true
  });

  AppRunCount.find({}).then(count => {
    if (Object.keys(count).length === 0) {
      superAdminRole.save();
      adminRole.save();
      agentRole.save();
      investorRole.save();
      male.save();
      female.save();
      superAdmin.save();

      fistTimeRun.save();
      console.log("initial data is saved");
    } else {
      console.log("This app has ran before");
    }
  });
};
