const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

const initializer = require("./initializer");
const loginController = require("./controllers/loginController");
const registerController = require("./controllers/registerController");
const termsController = require("./controllers/termsController");
const gender = require("./api/gender");

dotenv.config();
const app = express();

mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true
});
const PORT = process.env.PORT || 8000;
// set up template engine
app.set("view engine", "ejs");
// initialize roles and default user data and gender
initializer();
// api routes
app.use("/api/gender", gender);
// map static files
app.use(express.static("./public"));
app.use("/login", loginController);
app.use("/register", registerController);
app.use("/terms", termsController);
// use home page.
app.get("/", (req, res) => {
  res.render("index");
});

app.listen(PORT, () => console.log("Listening on port 8000"));
